import java.io.*;
import java.lang.reflect.Array;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;

public class Client {
    private static final int PACKET_SIZE = 65536;//8192;
    private String fileName;
    private InetAddress ipAddress;
    private int serverPort;
    Client(String fileName, String serverAddress, String serverPort) {
        this.fileName = fileName;
        try {
            this.ipAddress = InetAddress.getByName(serverAddress);
        }
        catch (UnknownHostException ex) {
            ex.printStackTrace();
            System.exit(0);
        }
        this.serverPort = Integer.valueOf(serverPort);
    }

    public void start() {
        System.out.println("File \"" + fileName + "\" upload started.");
        Socket clientSocket = null;
        OutputStream sout  = null;
        InputStream sin  = null;
        try {
            clientSocket = new Socket(ipAddress, serverPort);
                System.out.println("File name: " + fileName);
            File file = new File(fileName);
            String lastFileName = file.getName();
                System.out.println("Last File name: " + lastFileName);
            byte[] nameBytes = lastFileName.getBytes("UTF-8");
                System.out.println("Array Length: " + Integer.toString(nameBytes.length));
                System.out.println("File name bytes: " + Arrays.toString(nameBytes));
            sout = clientSocket.getOutputStream();
            sin  =  clientSocket.getInputStream();
            DataOutputStream dout = new DataOutputStream(sout);
            DataInputStream din = new DataInputStream(sin);
            dout.writeInt(nameBytes.length); // File name length
            dout.write(nameBytes); // File name in bytes
//            dout.writeUTF(lastFileName);
            long fileSize = file.length();
            dout.writeLong(fileSize); // File size
            dout.writeInt(PACKET_SIZE); // Packet size
            int bytesRead = 0;
            long totalBytesRead = 0;
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(fis);
            BufferedOutputStream bos = new BufferedOutputStream(sout);
            byte buf[] = new byte[PACKET_SIZE];
            while ((bytesRead = bis.read(buf, 0, PACKET_SIZE)) != -1){
                bos.write(buf, 0, bytesRead);
                totalBytesRead += bytesRead;
                if (totalBytesRead == fileSize) break;
            }
            if (totalBytesRead != fileSize) {
                System.out.println("Read file size doesn't equal original file size.");
            }
            bos.flush();
            boolean result = sin.read() == 1;
            System.out.println("Upload file  " + (result ? "successful" : "failed"));
            bos.close();
            dout.close();
            sout.close();
            bis.close();
            fis.close();
            din.close();
            sin.close();
            clientSocket.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
